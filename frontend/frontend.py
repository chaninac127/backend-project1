from jinja2 import Template
from flask import Flask, render_template, jsonify, redirect
import json
# import simplejson
import os
import requests

app = Flask(__name__)

@app.route('/')
def index():
    return bucketlist()

@app.route('/bucketlist',methods=['GET'])
def bucketlist():
    r = requests.get(f"http://localhost:5000/debugger")
    bucketlist = (r.json()['collections'])
    return render_template('m3BL.html',list_example=bucketlist)

@app.route('/<bucketname>/controlcenter',methods=['GET'])
def controlcenter(bucketname):
    r = requests.get(f"http://localhost:5000/{bucketname}?list")
    itemlist = (r.json()['list'])
    movies = [item for item in itemlist if (item.endswith('.mp4') or item.endswith('.avi') or item.endswith('.mov'))]
    return render_template('m3CC.html',alert="",bucketname=bucketname,list_example=movies)

@app.route('/<bucketname>/<objectname>/submit',methods=['POST'])
def submitone(bucketname,objectname):
    print("===================IN HERE====================")
    name, ext = os.path.splitext(objectname)
    outputname = str(name) + '.gif'
    body = {"bucketname" : bucketname, "inputfile" : objectname, "outputfile" : outputname}
    print("the body: ",body)
    try:
        requests.post(f"http://localhost:8080/submit",json=body)
    except Exception as error:
        print("Error : ",error)
    return redirect(f"http://localhost:8030/{bucketname}/controlcenter")

@app.route('/<bucketname>/submitall',methods=['POST'])
def submitall(bucketname):
    requests.post(f"http://localhost:8080/{bucketname}/createjobs") 
    return redirect(f"http://localhost:8030/{bucketname}/controlcenter")


@app.route('/<bucketname>/displayroom',methods=['GET'])
def show(bucketname):
    r = requests.get(f"http://localhost:8080/{bucketname}/listgifs")
    giflist = (r.json()['giflist'])
    return render_template('m3DR.html',alert="",bucketname=bucketname,deleteall=f"http://localhost:8030/{bucketname}/cleargifs",list_example=[(gif,f"http://localhost:5000/{bucketname}/{gif}") for gif in giflist])

@app.route('/<bucketname>/<objectname>/delete',methods=['POST'])
def delete_one(bucketname,objectname):
    try:
        requests.delete(f"http://localhost:5000/{bucketname}/{objectname}?delete")
    except Exception as error:
        print("Error : ", error)
    r = requests.get(f"http://localhost:8080/{bucketname}/listgifs")
    giflist = (r.json()['giflist'])
    if (objectname in giflist):
        giflist.remove(objectname)
    return render_template('m3DR.html',alert=f"deleted {objectname} from {bucketname}",bucketname=bucketname,deleteall=f"http://localhost:8030/{bucketname}/cleargifs",list_example=[(gif,f"http://localhost:5000/{bucketname}/{gif}") for gif in giflist])
    


@app.route('/<bucketname>/cleargifs',methods=['POST'])
def delete_gifs(bucketname):
    print("request :")
    r = requests.get(f"http://localhost:8080/{bucketname}/listgifs")
    giflist = (r.json()['giflist'])
    for gif in giflist:
        print("-----------------------------------------------------")
        print("=========before inner request===========")
        try:
            requests.delete(f"http://localhost:5000/{bucketname}/{gif}?delete")
        except Exception as error:
            print("Error detected",error)
        print("=========afer inner request===========")
        print("-----------------------------------------------------")
    after = requests.get(f"http://localhost:8080/{bucketname}/listgifs")
    giflistafter = (after.json()['giflist'])
    return render_template('m3DR.html',alert=f"deleted all gifs from {bucketname}",bucketname=bucketname,deleteall=f"http://localhost:8030/{bucketname}/cleargifs",list_example=[(gif,f"http://localhost:5000/{bucketname}/{gif}") for gif in giflistafter])

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


if __name__ == '__main__':
    app.run(debug=True, port=8030)