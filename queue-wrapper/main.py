#go read l3-work-queue/simple-redisq-v2

import os
import json
import redis
import requests
import logging
from flask import Flask, jsonify, request, abort

LOG = logging
LOG.basicConfig(
    level=LOG.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)

STATUS_OK = requests.codes['ok']

app = Flask(__name__)

class RedisResource:
    REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'localhost')
    QUEUE_NAME = 'queue:job_queue'
    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_= port_info
        port = (int(port),)

    conn = redis.Redis(host=host, *port)

@app.route('/submit', methods=['POST'])
def submit_job():
    body = request.json
    LOG.info('body : %s', body)
    json_packed = json.dumps(body)
    LOG.info('packed : %s', json_packed)
    print('packed : %s', json_packed)
    RedisResource.conn.rpush(
        RedisResource.QUEUE_NAME,
        json_packed
    )
    return jsonify({'status':'OK','job':json_packed})

@app.route('/<bucketname>/createjobs',methods=['POST'])
def submit_all_videos_for_queue(bucketname):
    #considering that all filename/path are string, use .endswith('.(file extension)')
    bucketinfo = requests.get('http://sos:5000/{}?list'.format(bucketname))
    if not bucketinfo.status_code == STATUS_OK:
        abort(400,'error getting bucketinfo')
    itemsinbucket = bucketinfo.json().get('complete')
    # itemsinbucket = bucketinfo['complete']
    for item in itemsinbucket:
        filename, fileextension = os.path.splitext(item)
        if fileextension in ['.mp4','.mov','.avi']:
            inputfile = str(filename + fileextension)
            gifname = str(filename + '.gif')
            tosent = {'inputfile' : inputfile ,'outputfile' : gifname,'bucketname' : bucketname}
            json_packed = json.dumps(tosent)
            # requests.post("queue-wrapper:8080/submit",data=taskdesc)
            # taskdesc = json.dumps({'inputfile' : inputfile ,'outputfile' : gifname,'bucketname' : bucketname})
            RedisResource.conn.rpush(
                RedisResource.QUEUE_NAME,
                json_packed
            )
    return jsonify({'status':'OK','iteminbuckets' : itemsinbucket})
    

@app.route('/<bucketname>/listgifs',methods=['GET'])
def list_gif_animations(bucketname):
    bucketinfo = requests.get('http://sos:5000/{}?list'.format(bucketname))
    if not bucketinfo.status_code == STATUS_OK:
        abort(400,'error getting bucketinfo')
    itemsinbucket = bucketinfo.json().get('complete')
    giflist = []
    for item in itemsinbucket:
        if item.endswith('.gif'):
            giflist.append(item)
    return jsonify({'giflist' : giflist})

