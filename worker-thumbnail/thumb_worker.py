#!/usr/bin/env python3

import sys
import moviepy.editor as mpy
import os
import logging
import json
import uuid
import redis
import hashlib
import requests
from werkzeug.datastructures import Headers

LOG = logging
REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'localhost')
QUEUE_NAME = 'queue:job_queue'

INSTANCE_NAME = uuid.uuid4().hex

LOG.basicConfig(
    level=LOG.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)

def watch_queue(redis_conn, queue_name, callback_func, timeout=30):
    active = True

    while active:
        # Fetch a json-encoded task using a blocking (left) pop
        packed = redis_conn.blpop([queue_name], timeout=timeout)

        if not packed:
            # if nothing is returned, poll a again
            continue

        _, packed_task = packed

        # If it's treated to a poison pill, quit the loop
        if packed_task == b'DIE':
            active = False
        else:
            task = None
            try:
                task = json.loads(packed_task.decode('utf-8'))
            except Exception:
                LOG.exception('json.loads failed')
            if task:
                callback_func(task)

def execute_task(log, task): #change this one to video and output
    bucketname = task.get('bucketname')
    inputfile = task.get('inputfile')
    outputfile = task.get('outputfile')
    LOG.info("=======================INSIDE WORKER============================")
    LOG.info("bucketname : %s", bucketname)
    LOG.info("inputfile : %s", inputfile)
    LOG.info("outputfile : %s", outputfile)
    LOG.info("================================================================")

    if inputfile and outputfile and bucketname:

        #make the directory
        if not (os.path.exists('/gifmaker/videos')):
            os.makedirs('/gifmaker/videos')
        if not (os.path.exists('/gifmaker/gifs')):
            os.makedirs('/gifmaker/gifs')

        inputLocalLocation = '/gifmaker/videos/{}'.format(inputfile)
        outputLocalLocation = '/gifmaker/gifs/{}'.format(outputfile)

        #the gif was already made just upload it back
        if not os.path.exists(outputLocalLocation):
            #download the video
            log.info('Downloading video from %s video name : %s',bucketname,inputfile)
            filetoconvert = requests.get('http://sos:5000/{}/{}'.format(bucketname,inputfile), stream=True)
            with open('/gifmaker/videos/{}'.format(inputfile),'wb') as f:
                for chunk in filetoconvert.iter_content(chunk_size=2048):
                    if chunk:
                        f.write(chunk)
            filetoconvert.close()
            log.info('Finished downloading as %s',inputLocalLocation)

            #make the gif
            log.info('Making thumbnail from %s to %s',inputLocalLocation,outputLocalLocation)
            # factors = [trial for trial in range(1, number+1) if number % trial == 0]
            makeGifFromMovie(inputLocalLocation,outputLocalLocation)
            log.info('Finished making gif file as %s',outputLocalLocation)
        else:
            log.info('file already exist : %s',outputLocalLocation)

        #upload the gif back
        log.info('outputLocalLocation : %s', outputLocalLocation)
        with open(outputLocalLocation,'rb') as r:
            gifbinary = r.read()
            md5forheader = hashlib.md5(gifbinary).hexdigest()
            log.info('md5 %s',md5forheader)
            log.info('creating a ticker for %s',outputfile)
            posting = requests.post('http://sos:5000/{}/{}?create'.format(bucketname,outputfile))
            log.info('posting : %d',posting.status_code)
            header = Headers()
            header.add('Content-MD5',md5forheader)
            log.info('uploading the gif')
            putting = requests.put('http://sos:5000/{}/{}?partNumber=1'.format(bucketname,outputfile),data=gifbinary,headers=header)
            log.info('putting : %d',putting.status_code)
            # r.close()
            log.info('completing the object')
            completing = requests.post('http://sos:5000/{}/{}?complete'.format(bucketname,outputfile))
            log.info('completing : %d',completing.status_code)
        
        log.info('Done, thumbnail making')
    else:
        log.info('No inputfile given.')

def makeGifFromMovie(inputname,outputname):
    LOG.info('insidemakeGifFromMovie')
    LOG.info('output : %s', outputname)
    LOG.info('input : %s', inputname)
    if not os.path.exists(inputname):
        LOG.info('inputfile not found GG!')
    raw_clip = mpy.VideoFileClip(inputname)
    startstamp = raw_clip.duration * 2/3
    duration = 10
    if(raw_clip.duration - startstamp) < 10:
        duration = raw_clip.duration - startstamp
    wanted_part = raw_clip.subclip(startstamp,startstamp+duration).resize((720,480))
    wanted_part.write_gif(outputname,fps=30)
    LOG.info('finished making Gif file')

def main():
    LOG.info('Starting a worker...')
    LOG.info('Unique name: %s', INSTANCE_NAME)
    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    named_logging = LOG.getLogger(name=INSTANCE_NAME)
    named_logging.info('Trying to connect to %s [%s]', host, REDIS_QUEUE_LOCATION)
    redis_conn = redis.Redis(host=host, *port)
    watch_queue(
        redis_conn, 
        QUEUE_NAME, 
        lambda task_descr: execute_task(named_logging, task_descr))

if __name__ == '__main__':
    # inputname = sys.argv[1]
    # outputname = sys.argv[2]
    # makeGifFromMovie(inputname,outputname)
    main()